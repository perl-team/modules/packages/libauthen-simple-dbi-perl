libauthen-simple-dbi-perl (0.2-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Remove Xavier Oswald from Uploaders. (Closes: #824301)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 08 Jun 2022 17:46:34 +0100

libauthen-simple-dbi-perl (0.2-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 13:39:40 +0100

libauthen-simple-dbi-perl (0.2-3) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.
  * debian/control: remove DM-Upload-Allowed since Xavier Oswald is a DD
    now.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Declare the package autopkgtestable
  * Update to Standards-Version 3.9.6
  * Switch to dpkg source format 3.0 (quilt)
  * Add explicit build dependency on libmodule-build-perl
  * Mention the module name in the long description
  * Switch to debhelper v9 tiny debian/rules

 -- Niko Tyni <ntyni@debian.org>  Sat, 06 Jun 2015 20:26:57 +0300

libauthen-simple-dbi-perl (0.2-2) unstable; urgency=low

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields. Add XS-DM-Upload-Allowed: yes
  * debian/rules: delete /usr/lib/perl5 only if it exists (closes: #467678).
  * debian/watch: use dist-based URL.
  * Set Standards-Version to 3.7.3 (no changes).
  * debian/rules:
    - swap binary-arch and binary-indep, this package is arch:all
    - remove OPTIMIZE, this package is still arch:all
    - let install-stamp depend on build-stamp
    - install upstream changelog
    - don't install README anymore, it's just the text version of the POD
      documentation
  * Set debhelper compatibility level to 6.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Tue, 26 Feb 2008 21:39:00 +0100

libauthen-simple-dbi-perl (0.2-1) unstable; urgency=low

  * Initial release (Closes: #410673)

 -- Xavier Oswald <x.oswald@free.fr>  Mon, 12 Feb 2007 15:20:57 +0100
